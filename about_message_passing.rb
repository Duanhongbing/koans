require File.expand_path(File.dirname(__FILE__) + '/neo')

class AboutMessagePassing < Neo::Koan

	class MessageCatcher
		def caught?
			true
		end
	end

	def test_methods_can_be_called_directly
		mc = MessageCatcher.new

		assert mc.caught?
	end

	def test_methods_can_be_invoked_by_sending_the_message
		mc = MessageCatcher.new
		# send方法可以动态的根据名称调用函数
		assert mc.send(:caught?)
	end

	def test_methods_can_be_invoked_more_dynamically
		mc = MessageCatcher.new
		# public_send 和send类似 只不过不会调用私有方法
		assert mc.public_send("caught?")
		# 做了一个字符串的拼接 send方法可以动态的传递不同的消息
		assert mc.send("caught" +  "?")    # What do you need to add to the first string?
		assert mc.send("CAUGHT?".downcase)      # What would you need to do to the string?
	end

	def test_send_with_underscores_will_also_send_messages
		mc = MessageCatcher.new
		# send方法的别名 作用相同
		# send方法太过强大，可以调用任何方法，包括私有方法，使用public_send方法将能够尊重方法接受者的隐私权，可以用它来代替send方法。
		assert_equal true, mc.__send__(:caught?)

		# THINK ABOUT IT:
		#
		# Why does Ruby provide both send and __send__ ?
	end

	def test_classes_can_be_asked_if_they_know_how_to_respond
		mc = MessageCatcher.new
		# 和send方法类似 respond_to 是用来判断对象能否响应给定的消息(方法调用的本质就是传递消息给对象)
		assert_equal true, mc.respond_to?(:caught?)
		assert_equal false, mc.respond_to?(:does_not_exist)
	end

	# ------------------------------------------------------------------

	class MessageCatcher
		# *args 的作用就是将传入的参数打散组成数组
		def add_a_payload(*args)
			args
		end
	end

	def test_sending_a_message_with_arguments
		mc = MessageCatcher.new
		# 调用方法未传递任何参数 所以返回一个空的数组
		assert_equal [], mc.add_a_payload
		# 使用send 调用方法未传递参数
		assert_equal [], mc.send(:add_a_payload)
		# nil也是对象 所以返回的数组内同样包含它
		assert_equal [3, 4, nil, 6], mc.add_a_payload(3, 4, nil, 6)
		# send方法时同时将参数传入  send方法原型: obj.send(symbol [, args...])  可以看到第一个是一个symbol来作为方法名称,后面的都是参数
		assert_equal [3, 4, nil, 6], mc.send(:add_a_payload, 3, 4, nil, 6)
	end

	# NOTE:
	#
	# Both obj.msg and obj.send(:msg) sends the message named :msg to
	# the object. We use "send" when the name of the message can vary
	# dynamically (e.g. calculated at run time), but by far the most
	# common way of sending a message is just to say: obj.msg.

	# ------------------------------------------------------------------

	class TypicalObject
	end

	def test_sending_undefined_messages_to_a_typical_object_results_in_errors
		typical = TypicalObject.new
		# foobar 这个方法哪里来的？
		exception = assert_raise(NoMethodError) do
			typical.foobar
		end
		assert_match(/foobar/, exception.message)
	end

	def test_calling_method_missing_causes_the_no_method_error
		typical = TypicalObject.new
		# method_missing 方法的作用就是用来调用当前类中不存在的但其实在另外类中存在的  该方法在ruby沿着祖先琏寻找方法的时候一直找到最后一个仍旧未找到方法时 在抛出NomethodError前会调用已经定义好的method_missing 方法
		# 在本实例中未定义好method_missing方法 当然直接抛出异常
		exception = assert_raise(NoMethodError) do
			typical.method_missing(:foobar) 
		end
		assert_match(/foobar/, exception.message)

		# THINK ABOUT IT:
		#
		# If the method :method_missing causes the NoMethodError, then
		# what would happen if we redefine method_missing?
		#
		# NOTE:
		#
		# In Ruby 1.8 the method_missing method is public and can be
		# called as shown above. However, in Ruby 1.9 (and later versions)
		# the method_missing method is private. We explicitly made it
		# public in the testing framework so this example works in both
		# versions of Ruby. Just keep in mind you can't call
		# method_missing like that after Ruby 1.9 normally.
		#
		# Thanks.  We now return you to your regularly scheduled Ruby
		# Koans.
	end

	# ------------------------------------------------------------------

	class AllMessageCatcher
		# 这里就定义好了 method_missing 方法  如果你想调用其他类的方法 可以创建一个其他类的对象再使用send方法来进行调用 这里就没有那么做而是直接写了个伪造方法 可以说该类的对象能够调用任何方法
		def method_missing(method_name, *args, &block)
			"Someone called #{method_name} with <#{args.join(", ")}>"
		end
	end

	def test_all_messages_are_caught
		catcher = AllMessageCatcher.new

		assert_equal "Someone called foobar with <>", catcher.foobar
		assert_equal "Someone called foobaz with <1>", catcher.foobaz(1)
		assert_equal "Someone called sum with <1, 2, 3, 4, 5, 6>", catcher.sum(1,2,3,4,5,6)
	end

	def test_catching_messages_makes_respond_to_lie
		catcher = AllMessageCatcher.new
		# 定义了method_missing 就可以这么干
		assert_nothing_raised do catcher.any_method end # respond_to 方法只检查对象知道的方法(真真实实存在的方法)  类似于method_missing相关的方法所以是无法返回true的 assert_equal false, catcher.respond_to?(:any_method) end

		# ------------------------------------------------------------------
	end
	class WellBehavedFooCatcher
		def method_missing(method_name, *args, &block)
			if method_name.to_s[0,3] == "foo"
				"Foo to you too"
			else
				super(method_name, *args, &block)
			end
		end
	end

	def test_foo_method_are_caught
		catcher = WellBehavedFooCatcher.new
		# 前三位都是foo字符串
		assert_equal "Foo to you too", catcher.foo_bar
		assert_equal "Foo to you too", catcher.foo_baz
	end

	def test_non_foo_messages_are_treated_normally
		catcher = WellBehavedFooCatcher.new
		# 注意现在调用的方法的前三位是 nor 所以直接使用了super去调用 理所当然的 该方法无法被找到
		assert_raise(NoMethodError) do
			catcher.normal_undefined_method
		end
	end

	# ------------------------------------------------------------------

	# (note: just reopening class from above)
	class WellBehavedFooCatcher
		def respond_to?(method_name)
			if method_name.to_s[0,3] == "foo"
				true
			else
				super(method_name)
			end
		end
	end

	def test_explicitly_implementing_respond_to_lets_objects_tell_the_truth
		catcher = WellBehavedFooCatcher.new
		# 前三位是foo 直接返回true
		assert_equal true, catcher.respond_to?(:foo_bar)
		# 真实调用了respond_to 方法 所以返回false
		assert_equal false, catcher.respond_to?(:something_else)
	end
end
