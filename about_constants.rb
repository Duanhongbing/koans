require File.expand_path(File.dirname(__FILE__) + '/neo')

C = "top level"

class AboutConstants < Neo::Koan
 # 定义了一个常量
  C = "nested"

  def test_nested_constants_may_also_be_referenced_with_relative_paths
   # 常量的值
    assert_equal "nested", C
  end

  def test_top_level_constants_are_referenced_by_double_colons
    # ::可用来寻找常量  直接这样写::表示回到root namespace
	assert_equal "top level", ::C
  end

  def test_nested_constants_are_referenced_by_their_complete_path
    # 指定了类表示在类中寻找常量 
	assert_equal "nested", AboutConstants::C
   # 寻找root 下的AboutConstans类或者常量或者方法中的常量 
	assert_equal "nested", ::AboutConstants::C
  end

  # ------------------------------------------------------------------
  class Animal
    LEGS = 4
    def legs_in_animal
      LEGS
    end

    class NestedAnimal
      def legs_in_nested_animal
        LEGS
      end
    end
  end

  def test_nested_classes_inherit_constants_from_enclosing_classes
     # 创建Animal下的NestedAnimal对象 然后再调用legs_in_nested_animal方法
	  assert_equal 4, Animal::NestedAnimal.new.legs_in_nested_animal
  end

  # ------------------------------------------------------------------

  class Reptile < Animal
    def legs_in_reptile
      LEGS
    end
  end

  def test_subclasses_inherit_constants_from_parent_classes
    # 子类可以访问父类中的常量 
	assert_equal 4, Reptile.new.legs_in_reptile
  end

  # ------------------------------------------------------------------

  class MyAnimals
    LEGS = 2

    class Bird < Animal
      def legs_in_bird
        LEGS
      end
    end
  end

  def test_who_wins_with_both_nested_and_inherited_constants
    # 先寻找到了MyAnimal中的常量 
	assert_equal 2, MyAnimals::Bird.new.legs_in_bird
  end

  # QUESTION: Which has precedence: The constant in the lexical scope,
  # or the constant from the inheritance hierarchy?

  # ------------------------------------------------------------------

  class MyAnimals::Oyster < Animal
    def legs_in_oyster
      LEGS
    end
  end

  def test_who_wins_with_explicit_scoping_on_class_definition
     # 访问到了父类中的常量 
	assert_equal 4, MyAnimals::Oyster.new.legs_in_oyster
  end

  # QUESTION: Now which has precedence: The constant in the lexical
  # scope, or the constant from the inheritance hierarchy?  Why is it
  # different than the previous answer?
end
