require File.expand_path(File.dirname(__FILE__) + '/neo')

class AboutModules < Neo::Koan
  module Nameable
    def set_name(new_name)
      @name = new_name
    end

    def here
      :in_module
    end
  end

  def test_cant_instantiate_modules
    # Module 不能生成示例对象 
	assert_raise(NoMethodError) do
      Nameable.new
    end
  end

  # ------------------------------------------------------------------

  class Dog
    include Nameable

    attr_reader :name

    def initialize
      @name = "Fido"
    end

    def bark
      "WOOF"
    end

    def here
      :in_object
    end
  end

  def test_normal_methods_are_available_in_the_object
    # 正常方法调用
	fido = Dog.new
    assert_equal "WOOF", fido.bark
  end

  def test_module_methods_are_also_available_in_the_object
    fido = Dog.new
    assert_nothing_raised do
      # 将Module include 到类中后 就拥有了Module中的方法
      fido.set_name("Rover")
    end
  end

  def test_module_methods_can_affect_instance_variables_in_the_object
    fido = Dog.new
    # 构造方法 和 attr_reader 功劳
    assert_equal "Fido", fido.name
    fido.set_name("Rover")
    # 调用引入进来的Module内的方法
	assert_equal "Rover", fido.name
  end

  def test_classes_can_override_module_methods
    fido = Dog.new
    # override了Module中的方法
	assert_equal :in_object, fido.here
  end
end
