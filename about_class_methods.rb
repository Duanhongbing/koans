require File.expand_path(File.dirname(__FILE__) + '/neo')

class AboutClassMethods < Neo::Koan
  class Dog
  end
  
  # obj.is_a?(Kclass)这个方法用来判断Kclass是否是obj的类或者超类或者被mixin的模块
  def test_objects_are_objects
    # 任何对象的祖先琏内都有Object
	fido = Dog.new
    assert_equal true, fido.is_a?(Object)
  end

  def test_classes_are_classes
    # Dog的祖先琏内有Class
	assert_equal true, Dog.is_a?(Class)
  end

  def test_classes_are_objects_too
   # Dog祖先琏内有Object
	  assert_equal true, Dog.is_a?(Object)
  end

  def test_objects_have_methods
    #fido 本身是Dog类的实体对象，所以会包含该对象关系链内所有的方法
	fido = Dog.new
    assert fido.methods.size > 50 
  end

  def test_classes_have_methods
    # Dog 同样继承Object等祖先
	  assert Dog.methods.size > 50 
  end

  def test_you_can_define_methods_on_individual_objects
    fido = Dog.new
    def fido.wag
      :fidos_wag
    end
    # 定义了一个fido对象signleton_class 内的单件方法
	assert_equal :fidos_wag, fido.wag
  end

  def test_other_objects_are_not_affected_by_these_singleton_methods
    fido = Dog.new
    rover = Dog.new
    # wag方法是属于fido对象的singleton_class，其他对象访问不到该方法,所以会直接抛出NoMethodError异常
	def fido.wag
      :fidos_wag
    end

    assert_raise(NoMethodError) do
      rover.wag
    end
  end

  # ------------------------------------------------------------------

  class Dog2
   # 普通method
	 def wag
      :instance_level_wag
    end
  end
  # 类宏 
  def Dog2.wag
    :class_level_wag
  end

  def test_since_classes_are_objects_you_can_define_singleton_methods_on_them_too
    # 调用Dog2的类宏
	  assert_equal :class_level_wag, Dog2.wag
  end

  def test_class_methods_are_independent_of_instance_methods
    fido = Dog2.new
	# 调用的是类中的普通方法
    assert_equal :instance_level_wag, fido.wag
    # 调用Dog2的类宏
	assert_equal :class_level_wag, Dog2.wag
  end

  # ------------------------------------------------------------------

  class Dog
    # 该方法隐形定义了getter setter方法
	attr_accessor :name
  end

  def Dog.name
    @name
  end

  def test_classes_and_instances_do_not_share_instance_variables
    fido = Dog.new
    # 调用setter方法
	fido.name = "Fido"
    # 调用getter方法
	assert_equal "Fido", fido.name
    # 调用Dog的类宏,但未设置name的值，故返回nil
	assert_equal nil, Dog.name
  end

  # ------------------------------------------------------------------

  class Dog
    def Dog.a_class_method
      :dogs_class_method
    end
  end

  def test_you_can_define_class_methods_inside_the_class
    # 调用类宏
	  assert_equal :dogs_class_method, Dog.a_class_method
  end

  # ------------------------------------------------------------------

  LastExpressionInClassStatement = class Dog
                                     21
                                   end

  def test_class_statements_return_the_value_of_their_last_expression
    assert_equal 21, LastExpressionInClassStatement
  end

  # ------------------------------------------------------------------

  SelfInsideOfClassStatement = class Dog
                                 self
                               end

  def test_self_while_inside_class_is_class_object_not_instance
    # self 表示当前对象
	  assert_equal true, Dog == SelfInsideOfClassStatement
  end

  # ------------------------------------------------------------------

  class Dog
    # 定义了当前对象的单件方法
	 def self.class_method2
      :another_way_to_write_class_methods
    end
  end

  def test_you_can_use_self_instead_of_an_explicit_reference_to_dog
    assert_equal :another_way_to_write_class_methods, Dog.class_method2
  end

  # ------------------------------------------------------------------

  class Dog
    class << self
      def another_class_method
        :still_another_way
      end
    end
  end

  def test_heres_still_another_way_to_write_class_methods
    assert_equal :still_another_way, Dog.another_class_method
  end

  # THINK ABOUT IT:
  #
  # The two major ways to write class methods are:
  #   class Demo
  #     def self.method
  #     end
  #
  #     class << self
  #       def class_methods
  #       end
  #     end
  #   end
  #
  # Which do you prefer and why?
  # Are there times you might prefer one over the other?

  # ------------------------------------------------------------------

  def test_heres_an_easy_way_to_call_class_methods_from_instance_methods
    fido = Dog.new
    assert_equal :still_another_way, fido.class.another_class_method
  end

end
